package com.example.project;

import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.RootMatchers;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.doubleClick;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;

@RunWith(AndroidJUnit4.class)
public class AppTest2 {
    @Rule
    public ActivityTestRule<Packing_List> activityActivityTestRule = new ActivityTestRule<Packing_List>(Packing_List.class);

    @Test
    public void testscreen(){

        Espresso.onView(withId(R.id.fab_add)).perform(click());
        Espresso.onView(withId(R.id.itemname)).perform(clearText(), typeText("ToothBrush"));
        Espresso.onView(withId(R.id.plus)).perform(doubleClick());
        Espresso.onView(withId(R.id.addbuton)).perform(click());

    }


}
