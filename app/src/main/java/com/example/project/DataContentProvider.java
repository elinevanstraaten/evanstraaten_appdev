package com.example.project;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class DataContentProvider extends ContentProvider {
    private static final String AUTHORITY = "com.example.project";
    public static final String ITEMS_TABLE = "items";
    public static final Uri URI_PATH = Uri.parse("content://" + AUTHORITY + "/" + ITEMS_TABLE);
    private SQLiteDatabase db;

    private DatabaseHelper DbHelper;
    public static final int ITEM_ID = 101;
    public static final int ALL = 100;

    private static final UriMatcher urimatcher = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, ITEMS_TABLE, ALL);
        uriMatcher.addURI(AUTHORITY, ITEMS_TABLE + "/#", ITEM_ID);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        DbHelper = new DatabaseHelper(context);
        //        //db = DbHelper.getWritableDatabase();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(DatabaseFeeder.DatabaseFeed.TABLE_NAME);
        int urim = urimatcher.match(uri);
        switch (urim) {
            case ITEM_ID:
                queryBuilder.appendWhere(DatabaseFeeder.DatabaseFeed._ID + "=" + uri.getLastPathSegment());
                break;
            case ALL:
                break;
            default:
                throw new IllegalArgumentException("Unknown");
        }
        Cursor cursor = queryBuilder.query(DbHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase db = DbHelper.getWritableDatabase();
        int urim = urimatcher.match(uri);
        Uri new_uri = uri;
        switch (urim) {
            case ALL:
                long id = db.insert(DatabaseFeeder.DatabaseFeed.TABLE_NAME, null, values);
                new_uri = ContentUris.withAppendedId(uri, id);
                break;
        }
//        getContext().getContentResolver().notifyChange(uri, null);
        db.close();
        return new_uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType = urimatcher.match(uri);
        DatabaseHelper db = new DatabaseHelper(getContext());
        SQLiteDatabase sqlDB = db.getWritableDatabase();
        int rowsDeleted = 0;

        switch (uriType) {
            case ALL:
                rowsDeleted = sqlDB.delete(DatabaseFeeder.DatabaseFeed.TABLE_NAME,
                        selection,
                        selectionArgs);
                break;

            case ITEM_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(DatabaseFeeder.DatabaseFeed.TABLE_NAME,
                            DatabaseFeeder.DatabaseFeed._ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(DatabaseFeeder.DatabaseFeed.TABLE_NAME,
                            DatabaseFeeder.DatabaseFeed._ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int urim = urimatcher.match(uri);
        SQLiteDatabase DB = DbHelper.getWritableDatabase();
        int rowsUpdated = 0;

        switch (urim) {
            case ALL:
                rowsUpdated = DB.update(DatabaseFeeder.DatabaseFeed.TABLE_NAME, values,
                        selection,
                        selectionArgs);
                break;
            case ITEM_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = DB.update(DatabaseFeeder.DatabaseFeed.TABLE_NAME, values,
                            DatabaseFeeder.DatabaseFeed._ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = DB.update(DatabaseFeeder.DatabaseFeed.TABLE_NAME, values,
                            DatabaseFeeder.DatabaseFeed._ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }
}
