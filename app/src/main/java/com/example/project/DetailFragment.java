package com.example.project;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.project.DatabaseFeeder.DatabaseFeed.CONTENT_URI;


public class DetailFragment extends Fragment {
    private EditText EditIt;
    private TextView EditAmount;
    private static String n;
    private static String a;
    private static String ID;
    private int am;
    private OnFragmentInteractionListener mListener;

    public DetailFragment() {
        // Required empty public constructor
    }

    public static DetailFragment newInstance(String param1, String param2) {
        DetailFragment fragment = new DetailFragment();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        EditIt = view.findViewById(R.id.edititem);
        EditAmount = view.findViewById(R.id.editamount);
        Button edit = view.findViewById(R.id.editbutton);
        Button plus = view.findViewById(R.id.editplus);
        Button minus = view.findViewById(R.id.editminus);
        EditIt.setText(n);
        EditAmount.setText(a);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increase();
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrease();
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateItem(ID);

            }
        });


        return view;

    }

    public void EditItem(String id, String name, String amount) {
        n = name;
        a = amount;
        am = Integer.parseInt(amount);
        ID = id;


    }

    public void UpdateItem(String id) {
        ContentValues cv = new ContentValues();
        Intent intent = new Intent(this.getActivity(), Packing_List.class);
        String name = EditIt.getText().toString();
        String amount = EditAmount.getText().toString();
        cv.put(DatabaseFeeder.DatabaseFeed.COLUMN_NAME, name);
        cv.put(DatabaseFeeder.DatabaseFeed.COLUMN_AMOUNT, amount);
        this.getActivity().getApplicationContext().getContentResolver().update(CONTENT_URI, cv, DatabaseFeeder.DatabaseFeed._ID + "=" + id, null);
        startActivity(intent);

    }

    private void increase() {
        am++;
        EditAmount.setText(String.valueOf(am));

    }


    private void decrease() {
        if (am > 0) {
            am--;
            EditAmount.setText(String.valueOf(am));
        }

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
