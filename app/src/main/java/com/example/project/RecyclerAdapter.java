package com.example.project;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import static android.support.constraint.Constraints.TAG;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.viewholder> {

    private Context contxt;
    private Cursor crsr;
    public static ImageView img1;
    public static ImageView img2;
    private OnItemClickListener mListener;
    private static TextView select;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);

        void anotherItemClick(int pos);

        void onDeleteClick(int position);

        void onEditClick(int position);

    }

    public RecyclerAdapter(Context context, Cursor cursor) {
        contxt = context;
        crsr = cursor;

    }

    public static class viewholder extends RecyclerView.ViewHolder {
        public TextView mID;
        public TextView mamount;
        public TextView mitem;

        public viewholder(View v, final OnItemClickListener listener) {
            super(v);
            mID = v.findViewById(R.id.invisibleID);
            mamount = v.findViewById(R.id.Itemamount);
            mitem = v.findViewById(R.id.Itemname);
            img1 = v.findViewById(R.id.delete);
            img2 = v.findViewById(R.id.edit);
            select = v.findViewById(R.id.invisibleColor);
            img1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
            img2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {

                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onEditClick(position);
                        }
                    }
                }
            });

            //poging om items die aangeklikt zijn van kleur te veranderen

//            v.setOnClickListener(new View.OnClickListener(){
//                @Override
//                public void onClick(View v) {
//                    if (listener != null) {
//
//                        int position = getAdapterPosition();
//
//
//                       // String isclicked = listener.onItemClick(position);
//
//
////                        if (position != RecyclerView.NO_POSITION) {
////
////                            if (select.getText().toString().equals("notselected") ) {
////
////                                listener.onItemClick(position);
////
////                            }
////                            else{
////
////
////                                listener.anotherItemClick(position);
////
////
////                            }
////                        }
//                    }


//                }
//            });

        }
    }

    @Override
    public viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater infl = LayoutInflater.from(contxt);
        View v = infl.inflate(R.layout.list_item, parent, false);

        return new viewholder(v, mListener);

    }


    @Override
    public void onBindViewHolder(final viewholder holder, final int position) {
        if (!crsr.moveToPosition(position)) {
            return;
        }
        String name = crsr.getString(crsr.getColumnIndex(DatabaseFeeder.DatabaseFeed.COLUMN_NAME));
        int amount = crsr.getInt(crsr.getColumnIndex(DatabaseFeeder.DatabaseFeed.COLUMN_AMOUNT));
        String id = crsr.getString(crsr.getColumnIndex(DatabaseFeeder.DatabaseFeed._ID));
        holder.mID.setText(id);
        holder.mitem.setText(name);
        holder.mamount.setText(String.valueOf(amount));


        //holder.itemView.setTag(id);

    }


    @Override
    public int getItemCount() {

        return crsr.getCount();
    }

    public void newCursor(Cursor mcrsr) {
        if (crsr != null) {
            crsr.close();
        }
        crsr = mcrsr;
        if (mcrsr != null) {
            notifyDataSetChanged();
        }

    }


}

