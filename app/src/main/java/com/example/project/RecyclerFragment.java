package com.example.project;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import static com.example.project.DatabaseFeeder.DatabaseFeed.CONTENT_URI;

public class RecyclerFragment extends Fragment {
    private RecyclerAdapter mAdapter;
    private RecyclerView mList;
    private SwipeRefreshLayout swiperefresh;
    private EditText text;


    private OnFragmentInteractionListener mListener;

    public RecyclerFragment() {
        // Required empty public constructor
    }


    public static RecyclerFragment newInstance(String param1, String param2) {
        RecyclerFragment fragment = new RecyclerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        //  mList.setAdapter(mAdapter);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_list, container, false);
        text = view.findViewById(R.id.itemname);
        mList = view.findViewById(R.id.recycler);
        swiperefresh = view.findViewById(R.id.swipe);
        mAdapter = new RecyclerAdapter(this.getActivity(), getAllItems());
        mList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mList.setAdapter(mAdapter);
        mList.setHasFixedSize(true);


        //Swipe om pagina te herladen
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.newCursor(getAllItems());
                mAdapter.notifyDataSetChanged();
                swiperefresh.setRefreshing(false);
            }

        });


        //knop om item uit database te verwijderen

        mAdapter.setOnItemClickListener(new RecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                // mList.getChildAt(pos).setBackgroundColor(Color.rgb(0,255,0));
                TextView color = mList.findViewHolderForAdapterPosition(pos).itemView.findViewById(R.id.invisibleColor);
                color.setText("selected");
                mList.getChildAt(pos).setBackgroundColor(Color.rgb(255, 255, 0));


            }

            @Override
            public void anotherItemClick(int pos) {
                TextView color = mList.findViewHolderForAdapterPosition(pos).itemView.findViewById(R.id.invisibleColor);
                color.setText("notselected");
                mList.getChildAt(pos).setBackgroundColor(Color.rgb(255, 255, 255));


            }

            @Override
            public void onDeleteClick(int position) {
                TextView ID = mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.invisibleID);
                String Id = ID.getText().toString();

                //String name = ID.getText().toString();

                removeItem(Id);
            }

            @Override
            public void onEditClick(int position) {
                TextView ID = mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.invisibleID);
                TextView n = mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.Itemname);
                TextView am = mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.Itemamount);
                String Id = ID.getText().toString();
                String name = n.getText().toString();
                String amount = am.getText().toString();
                DetailFragment fragment = new DetailFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_detail, fragment).commit();
                //Data doorgeven om items aan te kunnen passen via contentprovider
                fragment.EditItem(Id, name, amount);
            }

        });
        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void removeItem(String id) {

        //data verwijderen met contentprovider
        getContext().getContentResolver().delete(Uri.parse(CONTENT_URI + "/" + id), null, null);


//            db.delete(DatabaseFeeder.DatabaseFeed.TABLE_NAME,
//                    DatabaseFeeder.DatabaseFeed._ID + " = " + id, null);


        mAdapter.newCursor(getAllItems());
    }

    private Cursor getAllItems() {

        //  return db.rawQuery(" SELECT * FROM " + DatabaseFeeder.DatabaseFeed.TABLE_NAME, null);

        //data ophalen met contentprovider
        Cursor cursor = getActivity().getApplicationContext().getContentResolver().query(CONTENT_URI, null, null, null, null);

        return cursor;
    }


}
