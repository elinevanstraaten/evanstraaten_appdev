package com.example.project;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.RemoteViews;

import static com.example.project.DatabaseFeeder.DatabaseFeed.CONTENT_URI;


/**
 * Implementation of App Widget functionality.
 */
public class RTGWidget extends AppWidgetProvider {

    @RequiresApi(api = Build.VERSION_CODES.O)
    void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                         int appWidgetId) {

        // Construct the RemoteViews object
        Intent intent = new Intent(context, MainActivity.class);
        Intent i = new Intent(context, MyIntentService.class);
        //pendingintent -> allows the foreign application to use your application's permissions to execute a predefined piece of code.
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        context.startService(i);
        //remoteviews -> A class that describes a view hierarchy that can be displayed in another process.
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.rtgwidget);
        views.setOnClickPendingIntent(R.id.widgetimg, pendingIntent);
        context.startService(i);
        Cursor c = context.getContentResolver().query(CONTENT_URI, null, null, null);

        if (c.moveToLast()) {
            String lastname = c.getString(c.getColumnIndex(DatabaseFeeder.DatabaseFeed.COLUMN_NAME));
            String lastamount = c.getString(c.getColumnIndex(DatabaseFeeder.DatabaseFeed.COLUMN_AMOUNT));
            views.setTextViewText(R.id.lastitem, lastname);
            views.setTextViewText(R.id.lastamount, lastamount);
            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);

        }


    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}
