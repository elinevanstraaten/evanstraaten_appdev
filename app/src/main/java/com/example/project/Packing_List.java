package com.example.project;

import android.app.ListFragment;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.TextView;


public class Packing_List extends AppCompatActivity implements RecyclerFragment.OnFragmentInteractionListener, AddItems.OnFragmentInteractionListener, DetailFragment.OnFragmentInteractionListener {

    private static TextView textV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {//

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packing_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        //DatabaseHelper dbhelper = new DatabaseHelper(this);
        //db = dbhelper.getWritableDatabase();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean lang = sharedPreferences.getBoolean("lang", false);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String loc = sp.getString(MainActivity.LOCVALUE, "location");
        String days = sp.getString(MainActivity.DAYVALUE, "days");
        setlanguage(lang, loc, days);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_additemstablet, new AddItems()).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment, new RecyclerFragment()).commit();

    }

    private void setlanguage(boolean lang, String loc, String days) {
        Intent intent = getIntent();
        textV = findViewById(R.id.where);
        if (lang) {
            textV.setText(getString(R.string.triptoNL) + " " + loc + " " + getString(R.string.forNL) + " " + days + " " + getString(R.string.daysNL));
        } else {
            textV.setText(getString(R.string.triptoENG) + " " + loc + " " + getString(R.string.forENG) + " " + days + " " + getString(R.string.daysENG));
        }

    }

    public void openPopUp(View v) {
        Intent intent = new Intent(this, Popup.class);

        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }


}
