package com.example.project;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Button;
import android.content.Intent;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, SharedPreferences.OnSharedPreferenceChangeListener {
    String[] Amountofdays = {"1", "2", "3", "4", "5"};

    public static final String LOCVALUE = "location";
    public static final String DAYVALUE = "days";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Spinner spin = (Spinner) findViewById(R.id.amountofdaysspinner);
        spin.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Amountofdays);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean lang = sharedPreferences.getBoolean("lang", false);
        setlanguage(lang);


        Button btn = (Button) findViewById(R.id.searchbutton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searching(v);
            }
        });


    }

    // Poging om settings toe te passen

    private void setlanguage(boolean lang) {
        EditText et = findViewById(R.id.searchlocation);
        Button bt = findViewById(R.id.searchbutton);
        TextView tv = findViewById(R.id.days);
        if (lang) {
            et.setHint(R.string.whereNL);
            bt.setText(R.string.searchNL);
            tv.setText(R.string.daysNL);

        } else {
            et.setHint(R.string.whereENG);
            bt.setText(R.string.searchENG);
            tv.setText(R.string.daysENG);
        }
    }

    public void searching(View view) {
        Intent intent = new Intent(this, Packing_List.class);
        EditText editbar = (EditText) findViewById(R.id.searchlocation);
        String loc = editbar.getText().toString();
        Spinner spin = (Spinner) findViewById(R.id.amountofdaysspinner);
        spin.setOnItemSelectedListener(this);
        String amount = spin.getSelectedItem().toString();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().putString(LOCVALUE, loc).apply();
        sp.edit().putString(DAYVALUE, amount).apply();
        //intent.putExtra("EXTRA_MESSAGE", loc);
        //intent.putExtra("AMOUNT", amount);
        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //openen settings menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, settings.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("lang")) {
            boolean lang = sharedPreferences.getBoolean("lang", false); //idk wat je default value is
            setlanguage(lang);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
