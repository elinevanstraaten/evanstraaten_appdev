package com.example.project;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.project.DatabaseFeeder.DatabaseFeed.CONTENT_URI;


public class AddItems extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private EditText addText;
    private static TextView addedAmount;
    private int amount = 0;
    private SQLiteDatabase db;
    private Toast mToast;

    private OnFragmentInteractionListener mListener;

    public AddItems() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static AddItems newInstance(String param1, String param2) {
        AddItems fragment = new AddItems();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_items, container, false);
        //       DatabaseHelper dbhelper;
//        dbhelper = new DatabaseHelper();
//        db = dbhelper.getWritableDatabase();
        addText = view.findViewById(R.id.itemname);
        addedAmount = view.findViewById(R.id.amount);

        Button plus = view.findViewById(R.id.plus);
        Button minus = view.findViewById(R.id.minus);
        Button add = view.findViewById(R.id.addbuton);

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increase();
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrease();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                additem();

            }
        });
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        //     boolean lang = sharedPreferences.getBoolean("lang", false);
        //    setlanguage(lang);
        return view;
    }

    private void setlanguage(boolean lang) {
        EditText et = this.getActivity().findViewById(R.id.itemname);
        Button bt = this.getActivity().findViewById(R.id.addbuton);

        if (lang) {
            et.setHint(R.string.whatNL);
            bt.setText(R.string.addNL);
        } else {
            et.setHint(R.string.whatENG);
            bt.setText(R.string.addENG);
        }
    }

    private void increase() {
        amount++;
        addedAmount.setText(String.valueOf(amount));

    }


    private void decrease() {
        if (amount > 0) {
            amount--;
            addedAmount.setText(String.valueOf(amount));
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void additem() {
        if (addText.getText().toString().trim().length() == 0 || amount == 0) {   //zorgen dat er niets leeg in de database komt
            return;
        }
        String name = addText.getText().toString();
        Intent intent = new Intent(this.getActivity(), Packing_List.class);
        ContentValues cv = new ContentValues();
        cv.put(DatabaseFeeder.DatabaseFeed.COLUMN_NAME, name);
        cv.put(DatabaseFeeder.DatabaseFeed.COLUMN_AMOUNT, amount);
        //Items toevoegen in db
        this.getActivity().getApplicationContext().getContentResolver().insert(CONTENT_URI, cv);
        String toastMessage = name + " added to list";
        mToast = Toast.makeText(this.getActivity(), toastMessage, Toast.LENGTH_LONG);
        mToast.show();
        addText.getText().clear();

        startActivity(intent);

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
