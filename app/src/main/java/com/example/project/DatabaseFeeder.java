package com.example.project;

import android.net.Uri;
import android.provider.BaseColumns;

import static com.example.project.DataContentProvider.ITEMS_TABLE;

public class DatabaseFeeder {
    private DatabaseFeeder() {
    }

    public static final class DatabaseFeed implements BaseColumns {

        public static final String TABLE_NAME = "items";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String AUTHORITY = "com.example.project";
        public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
    }
}
