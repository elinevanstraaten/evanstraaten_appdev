package com.example.project;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.example.project.DatabaseFeeder.*;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "items";
    public static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE = "CREATE TABLE " +
                DatabaseFeed.TABLE_NAME + " (" +
                DatabaseFeed._ID + " INTEGER PRIMARY KEY, " +
                DatabaseFeed.COLUMN_NAME + " TEXT NOT NULL, " +
                DatabaseFeed.COLUMN_AMOUNT + " INTEGER NOT NULL" +
                ")";
        db.execSQL(SQL_CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseFeed.TABLE_NAME);
        //onCreate(db);

    }
}
